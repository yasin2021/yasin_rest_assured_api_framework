************************Framework documentation**********************************************
CUCUMBER FRAMEWORK:
i)Test script development process
a)Download cucumber and testng dependencies in pom.xml file
b)Write test cases in feature file.
c)Create step definition of feature file and write java method of every test case.
d)Create cucumber.Options package
e)Create test runner class in cucumber.Option package and provide feature file path and step definition java class name 

ii)Reporting process
a)Download maven-cucumber-reporting dependencies in pom.xml file.
b)Create reporting path under target folder by provide path in test runner class having tag name as plugin.
c)Run the test using maven command mvn verify test.
d)Copy html tag inside \target\cucumber-html-reports\overview-features.html and check in browser.
e)For storing logs we define create log using filter method of request spec builder.

REST ASSURED API TEST SCRIPT DEVELOPMENT FLOW:


a)BasicConfiguration.java : 
i)Commonly used Request and Response header are set at one place by using RequestSpecBuilder and ResponseSpecBuilder class.
ii)Initialized object of Properties class to fetch the value form properties file.

b)REQUEST BODY
i)Using POJO(Plain Old Java Object) class - body(request pojo class object)
Create a java class for json body,Initialize key as string and generate getter and setter
For request payload - insert json value of every key using set method.
For response payload - get json value of every key using get method.
here response type will java object of response payload java class
 
syntax- ResponsePOJO obj = given()
                                .spec(RequestSpecification obj) //request spec builder
                                .body(RequestPojo obj)
                                .when()
                                .Post(getEnumConstants(resource)
                                .as(ResponsePOJO.class)
                                
                                
ii)Using HashMap
Define json body with map method by inserting key value inside put method and place the map object in body argument

Syntax - Response  response = given()
                                .spec(RequestSpecification obj) //request spec builder
                                .body(map object)
                                .when()
                                .Post(getEnumConstants(resource)
                                .then()
                                .spec(ResponseSpecification obj)
                                .extract()
                                .response();
 c)RESPONSE BODY
 i)using pojo
 Use ResponsePOJO obj to fetch the response body key and its value through obj.method process
 
 ii)HashMap
 Map<String,Object> responseBody = response.as(new TypeRef<Map<String,Object>>(){});                               
 Use responseBody to fetch fetch the response body key and its value through responseBody.get("key") process 
 
 iii)JsonPath class
 Initialize obj of JsonPath
 JsonPath js = new JsonPath(response.asString());
 js.get("jsonpath")                   
     
     
 d) TEST DATA
 i)Data are stored in properties file and fetch using properties class object.
 ii)Using testng xml parameterization        
 iii)Using Data Provider
                     
