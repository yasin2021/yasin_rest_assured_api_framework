package testngRestApiFramework;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;

import org.testng.annotations.BeforeSuite;


import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class BaseConfiguration {
	public static Properties prop;
	public static RequestSpecification req;
	public static RequestSpecification reqLib;
    public	ResponseSpecification res;
	
	
	
	public String getEnumConstants(String resources) {

		ResourceIdentifier resIden = ResourceIdentifier.valueOf(resources);

		return resIden.getResourecs();

	}
	

	
	
	@BeforeSuite
	public void GoogleConfigSetUp() throws FileNotFoundException {
    	requestSpecBuilder();
    	responseSpecBuilder();
    	
    	

	}
	
	@BeforeSuite
	public void LibraryConfigSetUp() throws FileNotFoundException {
		requestSpecBuilderForLibrary();
    	responseSpecBuilder();
    	
    	

	}
    
	public RequestSpecification requestSpecBuilder() throws FileNotFoundException {
		
		
		
		if (req == null) {
			PrintStream logs = new PrintStream(new FileOutputStream("apiLogFile.txt"));
		req = new RequestSpecBuilder()
				.setBaseUri(getEnumConstants("GoogleApiBaseUri"))
				.setContentType(ContentType.JSON)
				.addQueryParam("key", prop.getProperty("key"))
				.addFilter(RequestLoggingFilter.logRequestTo(logs))
				.addFilter(ResponseLoggingFilter.logResponseTo(logs))
				.build();
		         return req;
		}
		
		return req;

		
		
    }
	
	 public RequestSpecification requestSpecBuilderForLibrary() throws FileNotFoundException {
		    if (reqLib == null) {
				PrintStream logs = new PrintStream(new FileOutputStream("libraryAPI.txt"));
			reqLib = new RequestSpecBuilder()
					.setBaseUri(getEnumConstants("LibraryApiBaseUri"))
					.setContentType(ContentType.JSON)
					.addFilter(RequestLoggingFilter.logRequestTo(logs))
					.addFilter(ResponseLoggingFilter.logResponseTo(logs))
					.build();
			
			         return reqLib;
			}
		    
		    return reqLib; 
		    
		    }
	
    
    public ResponseSpecification responseSpecBuilder() {
    	
    	res = new ResponseSpecBuilder()
				.expectStatusCode(200)
				.expectContentType(ContentType.JSON)
				.build();
    	
    	return res;
    }
   
	
	public static Properties propertyLoader() {

		{
			try {
				FileReader reader = new FileReader(
						"Y:\\eclipse-workspace\\RestAssuredAPIFramework\\src\\main\\resource\\apiTestdata.properties");
				prop = new Properties();
				try {
					prop.load(reader);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}

		return prop;
	}

}
