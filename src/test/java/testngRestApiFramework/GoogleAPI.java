package testngRestApiFramework;

import org.testng.annotations.Test;

import hashMapJsonBody.GoogleAPIPayLoaderUisngHashMap;
import io.qameta.allure.Step;
import io.restassured.common.mapper.TypeRef;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojoClasses.GoogleAddPlaceResponseBody_POJO;
import pojoClasses.GoogleGetPlaceResponseBody_POJO;
import pojoClasses.GoogleUpdatePlaceResponseBody_POJO;

import static io.restassured.RestAssured.*;

import java.util.Map;

public class GoogleAPI extends GoogleAPIRequestBody {

	GoogleAddPlaceResponseBody_POJO responseBody = new GoogleAddPlaceResponseBody_POJO();
	GoogleUpdatePlaceResponseBody_POJO updateResponseBody = new GoogleUpdatePlaceResponseBody_POJO();
	GoogleGetPlaceResponseBody_POJO getResponseBody = new GoogleGetPlaceResponseBody_POJO();
	GoogleAPIPayLoaderUisngHashMap mapping = new GoogleAPIPayLoaderUisngHashMap();
	
	@Test(groups = {"Regression"})
	@Step
	public void addUpdateGetGooglePlace_TYPE1() {
		RequestSpecification request = given()
				.spec(req)
				.body(jsonRequestBody());

		Response response = request.when()
				.post(getEnumConstants("PostPlaceInGoogle"))
				.then().spec(res)
				.extract()
				.response();
		
		String responseBody = response.asString();
		System.out.println("Response : "+responseBody);
		
		JsonPath js = new JsonPath(responseBody);
		String PLACE_ID = js.getString("place_id");
		System.out.println("place id of the response in type 1 is :" + PLACE_ID);
		   
		
		Response updateRes = given()
				            .spec(req)
		                    .queryParam("place_id", PLACE_ID)
		                    .body(jsonRequestBody_updatePlace(PLACE_ID))
		                    .when()
		                    .put(getEnumConstants("UpdatePlaceInGoogle"))
				            .then()
				            .spec(res)
				            .extract()
				            .response();
		
		String udpateResBody = updateRes.asString();
		System.out.println(udpateResBody);
		
		Response getResponse = given()
	               .spec(req)
	               .queryParam("place_id", PLACE_ID)
	               .when()
	               .get(getEnumConstants("GetPlaceInGoogle"))
	               .then()
	               .spec(res)
	               .extract()
	               .response();
       
		String getResponseBody = getResponse.asString();
        System.out.println(getResponseBody);

		
		
		
	}
	@Step
	@Test(groups = {"Unit test","sanity"})
	public void addUpdateGetGooglePlace_TYPE2() {
		responseBody = given()
				.spec(req)
				.body(jsonRequestBody())
				.expect().defaultParser(Parser.JSON)
				.when()
				.post(getEnumConstants("PostPlaceInGoogle"))
				.as(GoogleAddPlaceResponseBody_POJO.class);
		String placeid = responseBody.getPlace_id();

		System.out.println("place id of the response in type 2 is :" + placeid);
		
		updateResponseBody = given()
				            .spec(req)
				            .queryParam("place_id", placeid)
				            .body(jsonRequestBody_updatePlace(placeid))
				            .expect().defaultParser(Parser.JSON)
				            .when()
				            .put(getEnumConstants("UpdatePlaceInGoogle"))
				            .as(GoogleUpdatePlaceResponseBody_POJO.class);
		String responseMessage = updateResponseBody.getMsg();
		System.out.println(responseMessage);
		
		getResponseBody =	given()
                       .spec(req)
                       .queryParam("place_id", placeid)
                       .expect().defaultParser(Parser.JSON)
                       .when()
                       .get(getEnumConstants("GetPlaceInGoogle"))
                       .as(GoogleGetPlaceResponseBody_POJO.class);
		
		String updatedAddress = getResponseBody.getAddress();
		System.out.println("updatedAddress is  : "+updatedAddress);
				               
				               
				            
		
		
	}
	
	@Test (groups = {"sanity"})
	@Step
	public void addUpdateGetGooglePlace_TYPE3() {
		Response addPlaceResponse = given()
				              .spec(req)
				              .body(mapping.addPlacePayRequestBody())
				              .when()
				              .post(getEnumConstants("PostPlaceInGoogle"))
				              .then()
				              .spec(res)
				              .extract()
				              .response();
		
		Map<String,Object> addPlaceRespBody = addPlaceResponse.as(new TypeRef<Map<String,Object>>(){});
		
		String place_id = addPlaceRespBody.get("place_id").toString();
		
		Response updatePlaceResponse = given()
				                       .spec(req)
				                       .body(mapping.updatePlacePayRequestBody(place_id))
				                       .when()
				                       .put(getEnumConstants("UpdatePlaceInGoogle"))
				                       .then()
				                       .spec(res)
				                       .extract()
				                       .response();
		
		Map<String,Object> updatePlaceRespBody = updatePlaceResponse.as(new TypeRef<Map<String,Object>>(){});
		String responseMessage = updatePlaceRespBody.get("msg").toString();
		System.out.println(responseMessage);   
		
		Response getPlaceResponse = given()
				                         .spec(req)
				                         .queryParam("place_id", place_id)
				                         .when()
				                         .get(getEnumConstants("GetPlaceInGoogle"))
				                         .then()
				                         .spec(res)
				                         .extract()
				                         .response();
		Map<String,Object> getPlaceRespBody = getPlaceResponse.as(new TypeRef<Map<String,Object>>(){});
		String updatedAddress = getPlaceRespBody.get("address").toString();
		System.out.println("updatedAddress using hashMap  : "+updatedAddress);
	}
	
}
