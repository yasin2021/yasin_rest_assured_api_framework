package testngRestApiFramework;

import java.io.FileNotFoundException;
import java.util.Map;



import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import hashMapJsonBody.LibraryAPIPayLoaderUsingHashMap;
import io.restassured.common.mapper.TypeRef;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import pojoClasses.LibraryAddBookRequestBody_POJO;


import static io.restassured.RestAssured.*;

public class LibraryAPI extends BaseConfiguration {
	String resources = "AddBookIntoLibrary";
	LibraryAddBookRequestBody_POJO payload;
	static String bookId;
	LibraryAPIPayLoaderUsingHashMap maping = new LibraryAPIPayLoaderUsingHashMap();
	
	public LibraryAddBookRequestBody_POJO payloads(String isbn, int aisle) throws FileNotFoundException {
		propertyLoader();
		payload = new LibraryAddBookRequestBody_POJO();
		payload.setName( prop.getProperty("name.libraryApi"));
		payload.setIsbn(isbn);
		payload.setAisle(aisle);
		payload.setAuthor(prop.getProperty("author"));
		return payload;
	}

	@Test(dataProvider = "bookData")
	public void addAndDeleteBook(String isbnValue,int aisleValue) throws FileNotFoundException {

		Response response = given()
				.spec(reqLib)
				.body(payloads(isbnValue,aisleValue))
				.expect().defaultParser(Parser.JSON)
				.when()
				.post(getEnumConstants(resources))
				.then()
				.spec(res)
				.extract()
				.response();
		
		Map<String, Object> responseBody = response.as(new TypeRef<Map<String, Object>>() {});
	    bookId = responseBody.get("ID").toString();
		System.out.println(bookId);

		deleteBook();
	}
	
	
	public void deleteBook() {
		Response response= given()
				           .spec(reqLib)
				           .body(maping.deleteBookRequestPayload(bookId))
				           .when()
				           .post(getEnumConstants("DeleteBook"))
				           .then()
				           .spec(res)
				           .extract()
				           .response();
		Map<String,Object> deleteResponse = response.as(new TypeRef<Map<String, Object>>() {});
		
		System.out.println(deleteResponse.get("msg"));
		
	}
	
	@DataProvider(name = "bookData")
	public Object[][] getBooksData() {
		return new Object[][] {{"firstBook",100},
			                   {"secondBook",200},
			                   {"ThirdBook",300}};
		
	}
	
}
