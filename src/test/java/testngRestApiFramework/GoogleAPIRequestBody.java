package testngRestApiFramework;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import pojoClasses.GoogleAddPlaceRequestBody_POJO;

import pojoClasses.GoogleUpdatePlaceRequestBody_POJO;

import pojoClasses.Locations_POJO;

public class GoogleAPIRequestBody extends BaseConfiguration {
	
	GoogleAddPlaceRequestBody_POJO requestBody = new GoogleAddPlaceRequestBody_POJO();
	
	Locations_POJO location = new Locations_POJO();

	Properties testData = propertyLoader();
	
	String array1 = testData.getProperty("types.array.index.zero");
	String array2 = testData.getProperty("types.array.index.one");
	List<String> typeBody = new ArrayList<String>();
	
	
	GoogleUpdatePlaceRequestBody_POJO updatePlaceReqBody = new GoogleUpdatePlaceRequestBody_POJO();
		public Locations_POJO jsonLoactionBody() {
		location.setLat(Double.parseDouble(testData.getProperty("lat")));
		location.setLng(Double.parseDouble(testData.getProperty("lng")));
		return location;
	}
		
    
	

	public GoogleAddPlaceRequestBody_POJO jsonRequestBody() {
		
		requestBody.setName(testData.getProperty("name"));
		requestBody.setLocation(jsonLoactionBody());
		requestBody.setAccuracy(Integer.parseInt(testData.getProperty("accuracy")));
		requestBody.setPhone_number(testData.getProperty("phone_number"));
		requestBody.setAddress(testData.getProperty("address"));
		 typeBody.add(array1);
		 typeBody.add(array2);	
		requestBody.setTypes(typeBody);
		requestBody.setWebsite(testData.getProperty("website"));
		requestBody.setLanguage(testData.getProperty("language"));
		return requestBody;
	}
	
	public GoogleUpdatePlaceRequestBody_POJO jsonRequestBody_updatePlace(String place_id) {
		
		updatePlaceReqBody.setAddress(testData.getProperty("update.address"));
		updatePlaceReqBody.setKey(testData.getProperty("key"));
		updatePlaceReqBody.setPlace_id(place_id);
		return updatePlaceReqBody;
		
		
	}

}
