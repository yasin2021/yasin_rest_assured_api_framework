package testngRestApiFramework;

public enum ResourceIdentifier {
	
	GoogleApiBaseUri("https://rahulshettyacademy.com"),
	PostPlaceInGoogle("/maps/api/place/add/json"),
	UpdatePlaceInGoogle("/maps/api/place/update/json"),
	GetPlaceInGoogle("/maps/api/place/get/json"),
	DeletePlaceInGoogle("/maps/api/place/delete/json"),
	LibraryApiBaseUri("http://216.10.245.166"),
	AddBookIntoLibrary("/Library/Addbook.php"),
	DeleteBook("/Library/DeleteBook.php");
	private String resources;
	
	ResourceIdentifier(String resources) {
		this.resources = resources;
	}

	public String getResourecs() {
		return resources;
	}
}
