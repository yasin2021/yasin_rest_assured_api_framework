package hashMapJsonBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import testngRestApiFramework.BaseConfiguration;

public class GoogleAPIPayLoaderUisngHashMap extends BaseConfiguration {

	public Properties prop = propertyLoader();

	public Map<String, Object> addPlacePayRequestBody() {

		Map<String, Object> addplace = new HashMap<String, Object>();

		addplace.put("location", locations());
		addplace.put("accuracy", prop.getProperty("accuracy"));
		addplace.put("name", prop.getProperty("name"));
		addplace.put("phone_number", prop.getProperty("phone_number"));
		addplace.put("address", prop.getProperty("address"));

		String array1 = prop.getProperty("types.array.index.zero");
		String array2 = prop.getProperty("types.array.index.one");
		List<String> typeBody = new ArrayList<String>();
		typeBody.add(array1);
		typeBody.add(array2);

		addplace.put("types", typeBody);
		addplace.put("website", prop.getProperty("website"));
		addplace.put("language", prop.getProperty("language"));

		return addplace;
	}

	public Map<String, Object> locations() {
		Map<String, Object> locationData = new HashMap<String, Object>();
		locationData.put("lat", Double.parseDouble(prop.getProperty("lat")));
		locationData.put("lng", Double.parseDouble(prop.getProperty("lng")));
		return locationData;
	}
	
	public Map<String, Object> updatePlacePayRequestBody(String PLACE_ID) {
		
		Map<String,Object> updatePlace = new HashMap<String,Object>();
		updatePlace.put("place_id", PLACE_ID);
		updatePlace.put("address", prop.getProperty("update.address"));
		updatePlace.put("key", prop.getProperty("key"));
		
		return updatePlace;
		
	}
	
	public Map<String, Object> deletePlacePayRequestBody(String PLACE_ID) {
		Map<String,Object> deletePlace = new HashMap<String,Object>();
		deletePlace.put("place_id", PLACE_ID);
		return deletePlace;
	}
	
	
}
