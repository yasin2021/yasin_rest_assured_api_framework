package hashMapJsonBody;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import testngRestApiFramework.BaseConfiguration;

public class LibraryAPIPayLoaderUsingHashMap extends BaseConfiguration {

	Properties prop =propertyLoader();

	public Map<String, Object> addBookRequestPayload(Object isbnValue, Object aisleValue) {
		Map<String, Object> addBook = new HashMap<String, Object>();
		 
		addBook.put("name", prop.getProperty("name.libraryApi"));
		addBook.put("isbn", isbnValue);
		addBook.put("aisle", aisleValue);
		addBook.put("author", prop.getProperty("author"));
		return addBook;
	}

	public Map<String, Object> deleteBookRequestPayload(Object bookId) {
		Map<String, Object> deleteBook = new HashMap<String, Object>();
		deleteBook.put("ID", bookId);

		return deleteBook;
	}
}
