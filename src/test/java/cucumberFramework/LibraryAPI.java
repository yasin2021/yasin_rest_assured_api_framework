package cucumberFramework;

import java.io.FileNotFoundException;
import java.util.Map;


import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import hashMapJsonBody.LibraryAPIPayLoaderUsingHashMap;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.*;

public class LibraryAPI extends LibraryAPIPayLoaderUsingHashMap{
	
	Map<String,Object> addBookRequestBody;
	Map<String,Object> deleteBookRequestBody;
	RequestSpecification request;
	Response response;
	static String actualId;
	@Given("Prepare request payload body for add book test case with {string} {int}")
	public void prepare_request_payload_body_for_add_book_test_case_with (String isbn,int aisle) {
		
	addBookRequestBody = addBookRequestPayload(isbn,aisle);
	
	}

	@Given("Prepare request for Add Book test case")
	public void prepare_request_for_Add_Book_test_case() throws FileNotFoundException {
		
			RequestSpecification reqst = requestSpecBuilderForLibrary();
			request = given()
					.spec(reqst)
					.body(addBookRequestBody);
					
	}
	
	@When("Send {string} request for {string} test case")
	public void send_request_for_test_case(String htppRequest, String resources) {
		ResponseSpecification res = responseSpecBuilder();
		if(htppRequest.equalsIgnoreCase("Post")) {
			response = request.when()
			.post(getEnumConstants(resources))
			.then()
			.spec(res)
			.extract()
			.response();
		}
		
		if(htppRequest.equalsIgnoreCase("DELETE")) {
			response = request.when()
			.post(getEnumConstants(resources))
			.then()
			.spec(res)
			.extract()
			.response();
		}
	}

	@Then("validate correct id is displaying in the reponse with {string} {int}")
	public void validate_correct_id_is_displaying_in_the_reponse_with(String string, Integer int1) {
		Map<String,Object> addBookResponseBody = response.as(new TypeRef<Map<String,Object>>() {});
		
		actualId = addBookResponseBody.get("ID").toString();
		System.out.println("ID from response body is: "+actualId);
		
		String expectedId = string.concat(int1.toString());
		System.out.println("expected id is: "+expectedId);
		Assert.assertEquals(actualId, expectedId);
		
		

	}
	
	@Given("Prepare request payload body for delete book test case")
	public void prepare_request_payload_body_for_delete_book_test_case() {
	    
		deleteBookRequestBody = deleteBookRequestPayload(actualId);
	}
	@Given("Prepare request for Delete Book test case")
	public void prepare_request_for_Delete_Book_test_case() throws FileNotFoundException {
		
		
			RequestSpecification reqst = requestSpecBuilderForLibrary();
			request = given()
					.spec(reqst)
					.body(deleteBookRequestBody);
					
		
		
	}
	
	@Then("Validate response message")
	public void validate_response_message() {
		
		Map<String,Object> deleteBookResponseBody = response.as(new TypeRef<Map<String,Object>>() {});
		String message = deleteBookResponseBody.get("msg").toString();
		System.out.println(message);
		
	    
	}

}
