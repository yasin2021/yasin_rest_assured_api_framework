package cucumberFramework;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import hashMapJsonBody.GoogleAPIPayLoaderUisngHashMap;


import io.restassured.common.mapper.TypeRef;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.*;

import java.io.FileNotFoundException;

import java.util.Map;

import org.junit.runner.RunWith;
import org.testng.Assert;



@RunWith(Cucumber.class)
public class GoogleAPICucumber extends GoogleAPIPayLoaderUisngHashMap {
	
	RequestSpecification request;
	Response resp;
	static String placeId;
	static Map<String, Object> updatePlaceRequestBody;
	Map<String, Object> deletePlaceRequestBody;
	Map<String, Object> addplaceRequestBody;
	@Given("Prepare Request payload body for add place operation")
	public void prepare_Request_payload_body_for_add_place_operation() {
		addplaceRequestBody = addPlacePayRequestBody();
	}


	@Given("Prepare request for {string}")
	public void prepare_request_for(String scenarios)  throws FileNotFoundException {
		
		RequestSpecification reqst = requestSpecBuilder();
		if (scenarios.equalsIgnoreCase("Add place API")) {
	
	                  request = given()
			                    .spec(reqst)
			                    .body(addplaceRequestBody);

		}
		if (scenarios.equalsIgnoreCase("Update place API")) {

			request = given()
					.spec(reqst)
					.body(updatePlaceRequestBody);

		}
		
		if (scenarios.equalsIgnoreCase("Get place API")) {

			request = given()
					.spec(reqst)
					.queryParam("place_id", placeId);
					

		}
		if (scenarios.equalsIgnoreCase("Delete place API")) {

			request = given()
					.spec(reqst)
					.body(deletePlaceRequestBody);
					

		}

	}

	@When("Send {string} request for {string}")
	public void Send_request_for(String htppRequest, String resource) {
		ResponseSpecification res = responseSpecBuilder();
		if(htppRequest.equalsIgnoreCase("Post")) {
			     resp = request
					        .when()
					        .post(getEnumConstants(resource))
					        .then()
					        .spec(res)
					        .extract()
					        .response();
	        System.out.println("add place response body :"+resp.asString());   
	        
		}
		
		if(htppRequest.equalsIgnoreCase("Get")) {
			       resp = request
					        .when()
					        .get(getEnumConstants(resource))
					        .then()
					        .spec(res)
					        .extract()
					        .response();
			System.out.println("get place response body :" + resp.asString());
			 
			
		}
		
		if(htppRequest.equalsIgnoreCase("Put")) {
		       resp = request
				        .when()
				        .put(getEnumConstants(resource))
				        .then()
				        .spec(res)
				        .extract()
				        .response();
		       
			System.out.println("put place response body :" + resp.asString());
		
	}
	
		
	}

	@Then("Validate status code and contentType of the {string} response")
	public void Validate_status_code_and_contentType_of_the_response(String operations) {
	   if(operations.equalsIgnoreCase("add place")) {
		        
   
			Map<String, Object> responseBody = resp.as(new TypeRef<Map<String, Object>>() {});
			placeId = responseBody.get("place_id").toString();
			System.out.println("Place id is:  " + placeId);
			String Actualstatus = responseBody.get("status").toString();
			Assert.assertEquals(Actualstatus, "OK");
			
	   }
	   if (operations.equalsIgnoreCase("update place")) {
		           
		   Map<String, Object> responseBody = resp.as(new TypeRef<Map<String, Object>>() {});
		   String Actualmessage = responseBody.get("msg").toString();
		   System.out.println("update response message is : "+Actualmessage);
		   Assert.assertEquals(Actualmessage, "Address successfully updated");
	   }
	   
		if (operations.equalsIgnoreCase("get place")) {
			
			
			 Map<String, Object> responseBody = resp.as(new TypeRef<Map<String, Object>>() {});
			 String updateAddress = responseBody.get("address").toString();
			 System.out.println("Address of the place is :"+updateAddress);
			 Assert.assertEquals(updateAddress, updatePlaceRequestBody.get("address"));
		}
		
		if (operations.equalsIgnoreCase("delete place")) {
			 Map<String, Object> responseBody = resp.as(new TypeRef<Map<String, Object>>() {});
			 String Actualstatus = responseBody.get("status").toString();
				Assert.assertEquals(Actualstatus, "OK");
		}
	}
	

	@Given("Prepare Request payload body for update place operation")
	public void prepare_Request_payload_body_for_update_place_operation() {
	   
		updatePlaceRequestBody = updatePlacePayRequestBody(placeId);
	}
	
	@Given("Prepare Request payload body for delete place operation")
	public void prepare_Request_payload_body_for_delete_place_operation() {
	   
		deletePlaceRequestBody = deletePlacePayRequestBody(placeId);
	}

	
	
	
}
