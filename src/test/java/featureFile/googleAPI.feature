Feature: Add place,update place and get place in google through API

Scenario: Add place in google server through google API
Given Prepare Request payload body for add place operation
And Prepare request for "Add place API"
When Send "POST" request for "PostPlaceInGoogle"
Then Validate status code and contentType of the "add place" response

Scenario: Update Place in google server through google API
Given Prepare Request payload body for update place operation
And Prepare request for "Update place API"
When Send "Put" request for "UpdatePlaceInGoogle"
Then Validate status code and contentType of the "update place" response

Scenario: get Place in google server through google API
Given Prepare request for "Get place API"
When Send "Get" request for "GetPlaceInGoogle"
Then Validate status code and contentType of the "get place" response

Scenario: delete Place in google server through google API
Given Prepare Request payload body for delete place operation
And Prepare request for "Delete place API"
When Send "Post" request for "DeletePlaceInGoogle"
Then Validate status code and contentType of the "delete place" response
