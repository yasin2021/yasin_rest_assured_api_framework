Feature: Add books,get and delete books using library API
#mvn test -Dcucumber.Options="--tags@addBooks"

@addBooks
Scenario Outline: Adding book into the library using libraray API
Given Prepare request payload body for add book test case with "<isbn>" <aisle>
And Prepare request for Add Book test case
When Send "POST" request for "AddBookIntoLibrary" test case
Then validate correct id is displaying in the reponse with "<isbn>" <aisle>

Examples:
| isbn        |  aisle  |
| Arihant      |     35 |


@deleteBook
Scenario: Deleting book from Library
Given Prepare request payload body for delete book test case
And Prepare request for Delete Book test case
When Send "DELETE" request for "DeleteBook" test case
Then Validate response message