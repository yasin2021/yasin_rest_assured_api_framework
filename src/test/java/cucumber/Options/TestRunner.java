package cucumber.Options;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Y:\\eclipse-workspace\\RestAssuredAPIFramework\\src\\test\\java\\featureFile",plugin = "json:target/jsonReports/cucumber-report.json",glue = {"cucumberFramework" })
public class TestRunner extends AbstractTestNGCucumberTests {

}